/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.si.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeesite.common.collect.ListUtils;
import com.jeesite.common.collect.MapUtils;
import com.jeesite.modules.suggestion.util.Stage;
import com.jeesite.modules.suggestion.util.Status;
import com.jeesite.modules.sys.entity.Employee;
import com.jeesite.modules.sys.entity.EmployeePost;
import com.jeesite.modules.sys.entity.Office;
import com.jeesite.modules.sys.entity.User;
import com.jeesite.modules.sys.service.EmployeeService;
import com.jeesite.modules.sys.service.OfficeService;
import com.jeesite.modules.sys.utils.UserUtils;
import com.jeesite.modules.sys.web.user.EmpUserController;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.si.entity.Quest;
import com.jeesite.modules.si.service.QuestService;

import java.util.List;
import java.util.Map;

/**
 * 问题表Controller
 * @author zw
 * @version 2018-05-31
 */
@Controller
@RequestMapping(value = "${adminPath}/si/quest")
public class QuestController extends BaseController {

	@Autowired
	private QuestService questService;
	@Autowired
	private EmployeeService employeeService;
	@Autowired
	private OfficeService officeService;
	@Autowired
	private EmpUserController empUserController;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Quest get(String questionCode, boolean isNewRecord) {
		return questService.get(questionCode, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("si:quest:view")
	@RequestMapping(value = {"list", ""})
	public String list(Quest quest, Model model) {
		model.addAttribute("quest", quest);
		return "modules/si/questList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("si:quest:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Quest> listData(Quest quest, HttpServletRequest request, HttpServletResponse response) {
		Page<Quest> page = questService.findPage(new Page<Quest>(request, response), quest); 
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("si:quest:view")
	@RequestMapping(value = "form")
	public String form(Quest quest, Model model) {
		model.addAttribute("quest", quest);
		return "modules/si/questForm";
	}

	/**
	 * 保存问题表
	 */
	@RequiresPermissions("si:quest:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Quest quest) {
		if (quest.getIsNewRecord()) {
			User user = UserUtils.getUser();
			Employee employee = (Employee) user.getRefObj();
			if (employee == null) {
				return renderResult(Global.FALSE, text("保存失败，当前用户没有机构部门信息"));
			}
			Office userOffice = employee.getOffice();
			if (userOffice == null) {
				return renderResult(Global.FALSE, text("保存失败，当前用户没有机构部门信息"));
			}
			quest.setUserCode(user);
			quest.setDepartmentCode(userOffice);
			quest.setStage(String.valueOf(Stage.RAISE.getValue()));
			quest.setBzStatus(String.valueOf(Status.UNCOMMITED.getValue()));
		}
		questService.save(quest);
		return renderResult(Global.TRUE, text("保存问题表成功！"));
	}
	
	/**
	 * 删除问题表
	 */
	@RequiresPermissions("si:quest:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Quest quest) {
		questService.delete(quest);
		return renderResult(Global.TRUE, text("删除问题表成功！"));
	}

	/**
	 * 查看问题
	 * @param quest
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "show")
	public String show(Quest quest, Model model) {
		model.addAttribute("quest", quest);
		return "modules/si/questShow";
	}

	/**
	 * 提交申请->审核
	 * @param quest
	 * @param taskId
	 * @return
	 */
	@PostMapping(value = "submit")
	@ResponseBody
	public String submit(Quest quest, String taskId) {
		String myLeader = findMyLeader(quest.getDepartmentCode());
		if (myLeader == null) {
			return renderResult(Global.FALSE, text("提交失败，当前用户没有部门负责人"));
		}
		questService.submit(quest, taskId, myLeader);
		return renderResult(Global.TRUE, text("提交成功"));
	}

	/**
	 * 部门审核驳回
	 * @return
	 */
	@PostMapping(value = "auditReject")
	@ResponseBody
	public String auditReject(Quest quest, String taskId) {
		String rejectMsg = quest.getRejectMsg();
		if (StringUtils.isBlank(rejectMsg)) {
			return renderResult(Global.FALSE, text("请填写驳回理由"));
		}
		quest.setStage(String.valueOf(Stage.RAISE.getValue()));
		quest.setBzStatus(String.valueOf(Status.REJECT.getValue()));
		questService.next(quest, taskId, quest.getUserCode().getUserCode(),
				2, "部门审核驳回");
		return renderResult(Global.TRUE, text("提交成功"));
	}

	/**
	 * 部门审核通过
	 * @return
	 */
	@PostMapping(value = "auditPass")
	@ResponseBody
	public String auditPass(Quest quest, String taskId) {
		String myLeader = findMyLeader(quest.getDepartmentCode());
		if (myLeader == null) {
			return  renderResult(Global.TRUE, text("提交失败，部门{0}没有负责人", quest.getDepartmentCode().getFullName()));
		}
		quest.setStage(String.valueOf(Stage.DISPATCH.getValue()));
		quest.setQuestManager(new User(myLeader));
		questService.next(quest, taskId, myLeader, 1, "部门审核通过");
		return renderResult(Global.TRUE, text("提交成功"));
	}

	/**
	 * 拒绝指派
	 * @return
	 */
	@PostMapping(value = "receiveReject")
	@ResponseBody
	public String receiveReject(Quest quest, String taskId) {
		quest.setStage(String.valueOf(Stage.AUDIT.getValue()));
		questService.next(quest, taskId, findMyLeader(quest.getDepartmentCode()),
				2, "拒绝指派");
		return renderResult(Global.TRUE, text("提交成功"));
	}

	/**
	 * 接受并指派问题负责人，由问题负责人提出方案
	 * @return
	 */
	@PostMapping(value = "receivePass")
	@ResponseBody
	public String receivePass(Quest quest, String taskId) {
		quest.setStage(String.valueOf(Stage.EXECUTE.getValue()));
		questService.next(quest, taskId, quest.getUserCode().getUserCode(),
				1, "接受并指派问题负责人");
		return renderResult(Global.TRUE, text("提交成功"));
	}

	/**
	 * 处理人提出方案
	 * @return
	 */
	@PostMapping(value = "execute")
	@ResponseBody
	public String execute(Quest quest, String taskId) {
		questService.next(quest, taskId, quest.getUserCode().getUserCode(),
				0, "处理人提出方案");
		return renderResult(Global.TRUE, text("提交成功"));
	}

	/**
	 * 处理人确认方案
	 * @return
	 */
	@PostMapping(value = "confirm")
	@ResponseBody
	public String confirm(Quest quest, String taskId) {
		String myLeader = findMyLeader(quest.getDepartmentCode());
		if (myLeader == null) {
			return renderResult(Global.TRUE, text("提交失败, 部门{0}没有负责人", quest.getQuestionDeptCode().getFullName()));
		}
		quest.setStage(String.valueOf(Stage.CONFIRM.getValue()));
		questService.next(quest, taskId, myLeader, 0, "处理人确认方案");
		return renderResult(Global.TRUE, text("提交成功"));
	}

	/**
	 * 处理人部门领导不认可
	 * @return
	 */
	@PostMapping(value = "deptConfirmReject")
	@ResponseBody
	public String deptConfirmReject(Quest quest, String taskId) {
		quest.setStage(String.valueOf(Stage.EXECUTE.getValue()));
		questService.next(quest, taskId, quest.getUserCode().getUserCode(),
				2, "处理人部门领导不认可");
		return renderResult(Global.TRUE, text("提交成功"));
	}

	/**
	 * 处理人部门领导认可
	 * @return
	 */
	@PostMapping(value = "deptConfirmPass")
	@ResponseBody
	public String deptConfirmPass(Quest quest, String taskId) {
		quest.setStage(String.valueOf(Stage.FINIESH.getValue()));
		questService.next(quest, taskId, null, 1, "处理人部门领导认可");
		return renderResult(Global.TRUE, text("提交成功"));
	}

	/**
	 * 获取问题部门的普通员工
	 * @return
	 */
	@RequestMapping(value = "treeData")
	@ResponseBody
	public List<Map<String, Object>> treeData(String excludeCode, String parentCode, Boolean isAll,
											  String officeTypes, String companyCode, String officeCode, String isShowFullName,
											  Boolean isLoadUser, String postCode, String roleCode, String ctrlPermi) {
		List<Map<String, Object>> mapList = ListUtils.newArrayList();
		Office where = new Office();
		where.setStatus(Office.STATUS_NORMAL);
		where.setCompanyCode(companyCode);
		where.setOfficeCode(officeCode);
		List<Office> list = officeService.findList(where);
		for (int i = 0; i < list.size(); i++) {
			Office e = list.get(i);

			Map<String, Object> map = MapUtils.newHashMap();
			map.put("id", e.getId());
			map.put("pId", e.getParentCode());
			String name = e.getOfficeName();
			if ("true".equals(isShowFullName) || "1".equals(isShowFullName)){
				name = e.getFullName();
			}
			map.put("name", com.jeesite.common.lang.StringUtils.getTreeNodeName("false", e.getViewCode(), name));
			map.put("title", e.getFullName());
			// 一次性后台加载用户，提高性能(推荐方法)
			if (isLoadUser != null && isLoadUser) {
				map.put("isParent", true);
				List<Map<String, Object>> userList;
				userList = empUserController.treeData("u_", e.getOfficeCode(), e.getOfficeCode(),
						companyCode, postCode, roleCode, isAll, "false");
				mapList.addAll(userList);
			}
			mapList.add(map);
		}
		return mapList;
	}

	/**
	 * 查找部门负责人（部门经理）
	 * @param office
	 * @return
	 */
	private String findMyLeader(Office office) {
		Employee employee=new Employee();
		employee.setOffice(office);
		List<Employee> employees=employeeService.findList(employee);
		for(Employee emp:employees) {
			for(EmployeePost employeePost:employeeService.findEmployeePostList(emp)) {
				if("dept".equals(employeePost.getPostCode())) {
					return emp.getEmpCode();
				}
			}
		}
		return null;
	}
}