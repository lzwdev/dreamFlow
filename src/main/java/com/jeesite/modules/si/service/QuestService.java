/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.si.service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jeesite.modules.bpm.util.BpmUtils;
import com.jeesite.modules.suggestion.util.Stage;
import com.jeesite.modules.suggestion.util.Status;
import com.jeesite.modules.sys.utils.UserUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.si.entity.Quest;
import com.jeesite.modules.si.dao.QuestDao;

/**
 * 问题表Service
 * @author zw
 * @version 2018-05-31
 */
@Service
@Transactional(readOnly=true)
public class QuestService extends CrudService<QuestDao, Quest> {
	
	/**
	 * 获取单条数据
	 * @param quest
	 * @return
	 */
	@Override
	public Quest get(Quest quest) {
		return super.get(quest);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param quest
	 * @return
	 */
	@Override
	public Page<Quest> findPage(Page<Quest> page, Quest quest) {
		return super.findPage(page, quest);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param quest
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Quest quest) {
		super.save(quest);
	}
	
	/**
	 * 更新状态
	 * @param quest
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Quest quest) {
		super.updateStatus(quest);
	}
	
	/**
	 * 删除数据
	 * @param quest
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Quest quest) {
		super.delete(quest);
	}

	/**
	 * 提交申请->审核
	 * @param quest
	 * @param taskId
	 * @param nextCandidateUser
	 */
	@Transactional(readOnly=false, rollbackFor = Exception.class)
	public void submit(Quest quest, String taskId, String nextCandidateUser) {
		Map<String,Object> bpmParam=new HashMap<String,Object>();
		if (nextCandidateUser != null) {
			bpmParam.put("_candidateUser_", nextCandidateUser);
		}
		//首次提交
		if (quest.getBzStatus().equals(String.valueOf(Status.UNCOMMITED.getValue()))) {
			BpmUtils.startProcess("si_quest_manage", quest.getId(), quest.getQuestionTitle(),
					bpmParam, UserUtils.getUser().getId());
			quest.setCommitTime(new Date());
		} else {
			BpmUtils.completeTaskByIdAndAssignee(taskId, bpmParam,
					UserUtils.getUser().getId(), "重新提交", null);
		}
		quest.setBzStatus(String.valueOf(Status.UNAUDIT.getValue()));
		quest.setStage(String.valueOf(Stage.AUDIT.getValue()));
		super.save(quest);
	}

	/**
	 * 任务执行记录
	 * @param quest
	 * @param taskId
	 * @param nextCandidateUser 下一个审核人
	 * @param flag 表单参数
	 * @param deleteReason 审批说明
	 */
	@Transactional(readOnly = false)
	public void next(Quest quest, String taskId, String nextCandidateUser,
					 int flag, String deleteReason) {
		Map<String, Object> bpmParam = new HashMap<>();
		if (nextCandidateUser != null) {
			bpmParam.put("_candidateUser_", nextCandidateUser);
		}
		if (flag != 0) {
			bpmParam.put("flag", flag);
		}
		BpmUtils.completeTaskByIdAndAssignee(taskId, bpmParam,
				UserUtils.getUser().getId(), deleteReason, null);
		super.save(quest);
	}
}