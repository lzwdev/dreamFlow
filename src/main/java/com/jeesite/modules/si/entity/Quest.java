/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.si.entity;

import com.jeesite.modules.sys.entity.User;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.jeesite.modules.sys.entity.Office;
import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 问题表Entity
 * @author zw
 * @version 2018-05-31
 */
@Table(name="si_quest", alias="a", columns={
		@Column(name="question_code", attrName="questionCode", label="问题编码", isPK=true),
		@Column(name="user_code", attrName="userCode.userCode", label="提出问题员工编码"),
		@Column(name="department_code", attrName="departmentCode.officeCode", label="提出问题部门编码"),
		@Column(name="question_dept_code", attrName="questionDeptCode.officeCode", label="问题部门编码"),
		@Column(name="question_dept_name", attrName="questionDeptName", label="问题部门名称", queryType=QueryType.LIKE),
		@Column(name="question_title", attrName="questionTitle", label="问题标题", queryType=QueryType.LIKE),
		@Column(name="question_type", attrName="questionType", label="问题分类"),
		@Column(includeEntity=DataEntity.class),
		@Column(name="stage", attrName="stage", label="阶段", comment="阶段(1:建议提出, 2:等待审核, 3:等待指派, 4: 执行阶段, 5: 等待认可, 6: 完成)"),
		@Column(name="bz_status", attrName="bzStatus", label="业务状态", comment="业务状态(1:建议提出, 2:等待审核, 3:等待指派, 4: 执行阶段, 5: 等待认可, 6: 完成)"),
		@Column(name="commit_time", attrName="commitTime", label="commit_time"),
		@Column(name="reject_msg", attrName="rejectMsg", label="不通过理由"),
		@Column(name="quest_manager", attrName="questManager.userCode", label="问题部门负责人"),
		@Column(name="quest_user", attrName="questUser.userCode", label="问题负责人"),
		@Column(name="finish_time", attrName="finishTime", label="预计完成时间"),
		@Column(name="plan", attrName="plan", label="方案"),
	}, joinTable={
		@JoinTable(type=Type.LEFT_JOIN, entity=User.class, attrName="userCode", alias="u2",
			on="u2.user_code = a.user_code", columns={
				@Column(name="user_code", label="用户编码", isPK=true),
				@Column(name="user_name", label="用户名称", isQuery=false),
		}),
		@JoinTable(type=Type.LEFT_JOIN, entity=Office.class, attrName="departmentCode", alias="u3",
			on="u3.office_code = a.department_code", columns={
				@Column(name="office_code", label="机构编码", isPK=true),
				@Column(name="office_name", label="机构名称", isQuery=false),
		}),
		@JoinTable(type=Type.LEFT_JOIN, entity=Office.class, attrName="questionDeptCode", alias="u4",
			on="u4.office_code = a.question_dept_code", columns={
				@Column(name="office_code", label="机构编码", isPK=true),
				@Column(name="office_name", label="机构名称", isQuery=false),
		}),
		@JoinTable(type=Type.LEFT_JOIN, entity=User.class, attrName="questManager", alias="u18",
			on="u18.user_code = a.quest_manager", columns={
				@Column(name="user_code", label="用户编码", isPK=true),
				@Column(name="user_name", label="用户名称", isQuery=false),
		}),
		@JoinTable(type=Type.LEFT_JOIN, entity=User.class, attrName="questUser", alias="u19",
			on="u19.user_code = a.quest_user", columns={
				@Column(name="user_code", label="用户编码", isPK=true),
				@Column(name="user_name", label="用户名称", isQuery=false),
		}),
	}, orderBy="a.update_date DESC"
)
public class Quest extends DataEntity<Quest> {
	
	private static final long serialVersionUID = 1L;
	private String questionCode;		// 问题编码
	private User userCode;		// 提出问题员工编码
	private Office departmentCode;		// 提出问题部门编码
	private Office questionDeptCode;		// 问题部门编码
	private String questionDeptName;		// 问题部门名称
	private String questionTitle;		// 问题标题
	private String questionType;		// 问题分类
	private String stage;		// 阶段(1:建议提出, 2:等待审核, 3:等待指派, 4: 执行阶段, 5: 等待认可, 6: 完成)
	private String bzStatus;		// 业务状态(1:未提交, 2:未审核, 3:拒绝)
	private Date commitTime;		// 提交时间
	private String rejectMsg;		// 不通过理由
	private User questManager;		// 问题部门负责人
	private User questUser;		// 问题负责人
	private Date finishTime;		// 预计完成时间
	private String plan;		// 方案
	
	public Quest() {
		this(null);
	}

	public Quest(String id){
		super(id);
	}
	
	public String getQuestionCode() {
		return questionCode;
	}

	public void setQuestionCode(String questionCode) {
		this.questionCode = questionCode;
	}
	
	public User getUserCode() {
		return userCode;
	}

	public void setUserCode(User userCode) {
		this.userCode = userCode;
	}
	
	public Office getDepartmentCode() {
		return departmentCode;
	}

	public void setDepartmentCode(Office departmentCode) {
		this.departmentCode = departmentCode;
	}
	
	public Office getQuestionDeptCode() {
		return questionDeptCode;
	}

	public void setQuestionDeptCode(Office questionDeptCode) {
		this.questionDeptCode = questionDeptCode;
	}
	
	@Length(min=0, max=64, message="问题部门名称长度不能超过 64 个字符")
	public String getQuestionDeptName() {
		return questionDeptName;
	}

	public void setQuestionDeptName(String questionDeptName) {
		this.questionDeptName = questionDeptName;
	}
	
	@Length(min=0, max=255, message="问题标题长度不能超过 255 个字符")
	public String getQuestionTitle() {
		return questionTitle;
	}

	public void setQuestionTitle(String questionTitle) {
		this.questionTitle = questionTitle;
	}
	
	@Length(min=0, max=100, message="问题分类长度不能超过 100 个字符")
	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	
	@Length(min=0, max=1, message="阶段长度不能超过 1 个字符")
	public String getStage() {
		return stage;
	}

	public void setStage(String stage) {
		this.stage = stage;
	}
	
	@Length(min=0, max=1, message="1长度不能超过 1 个字符")
	public String getBzStatus() {
		return bzStatus;
	}

	public void setBzStatus(String bzStatus) {
		this.bzStatus = bzStatus;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getCommitTime() {
		return commitTime;
	}

	public void setCommitTime(Date commitTime) {
		this.commitTime = commitTime;
	}
	
	@Length(min=0, max=255, message="不通过理由长度不能超过 255 个字符")
	public String getRejectMsg() {
		return rejectMsg;
	}

	public void setRejectMsg(String rejectMsg) {
		this.rejectMsg = rejectMsg;
	}
	
	public User getQuestManager() {
		return questManager;
	}

	public void setQuestManager(User questManager) {
		this.questManager = questManager;
	}
	
	public User getQuestUser() {
		return questUser;
	}

	public void setQuestUser(User questUser) {
		this.questUser = questUser;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getFinishTime() {
		return finishTime;
	}

	public void setFinishTime(Date finishTime) {
		this.finishTime = finishTime;
	}
	
	@Length(min=0, max=255, message="方案长度不能超过 255 个字符")
	public String getPlan() {
		return plan;
	}

	public void setPlan(String plan) {
		this.plan = plan;
	}
	
}