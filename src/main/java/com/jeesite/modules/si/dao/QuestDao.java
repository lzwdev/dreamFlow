/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.si.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.si.entity.Quest;

/**
 * 问题表DAO接口
 * @author zw
 * @version 2018-05-31
 */
@MyBatisDao
public interface QuestDao extends CrudDao<Quest> {
	
}