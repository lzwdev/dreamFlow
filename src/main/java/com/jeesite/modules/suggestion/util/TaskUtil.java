package com.jeesite.modules.suggestion.util;

import com.jeesite.common.utils.SpringUtils;
import com.jeesite.modules.si.entity.Quest;
import com.jeesite.modules.si.service.QuestService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;

/**
 * Created by zw on 2018/6/1.
 * 创建工作流
 */
public class TaskUtil {

    public static Quest getQuest(String taskId) {
        TaskService taskService = SpringUtils.getBean(TaskService.class);
        RuntimeService runtimeService = SpringUtils.getBean(RuntimeService.class);
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        ProcessInstance processInstance = runtimeService.createProcessInstanceQuery()
                .processInstanceId(task.getProcessInstanceId()).singleResult();
        String bzKey = processInstance.getBusinessKey();
        QuestService questService = SpringUtils.getBean(QuestService.class);
        return questService.get(bzKey);
    }
}
